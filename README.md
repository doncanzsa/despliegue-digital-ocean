# Despliegue de Aplicacion Laravel Vue en Digital Ocean

### Espacio de Trabajo en Digital Ocean

Iniciamos creando un nuevo proyecto que contendrá nuestras aplicaciones
para ello, nos dirigimos a Digital Ocean, Iniciamos sesión, en el apartado de proyectos creamos nuestro proyecto
para este caso colocaremos de nombre **Proyectos Personales**

Una vez creado nuestro proyecto, vamos a iniciar con un nuevo **Droplet**
* Seleccionamos Distribucion de Ubuntu 22.10 x64.
* Puedes elegir el plan que necesites, en mi caso, plan basico $6.
* Selecciona tu Datacenter mas cercano.
* Vamos a elegir autenticacion por SSH, si en tu caso aun no has agregado una clave ssh, te sugiero que agreges una.
* Bajamos a la parte de **Choose a hostname** y colocamos el nombre que tendra nuestro droplet, en mi caso colocare **proyectos-laravel-vue**
* Finalmente damos click en __Create Droplet__


Una ves creado nuestro droplet podemos ingresar a el, mediante la opcion de consola que nos proporciona la pagina, o mediante la consola de nuestra maquina donde tengamos la clave ssh asignada.

Al ingresar a nuestro froplet aparecera algo como: 
```
root@proyectos-laravel-vue:~# 
```

### Configuracion de nuestro Droplet
ahora que estamos en nuestro servidor, vamos a configurarlo
```bash
# Actualizamos paquetes
$ sudo apt-get update
$ sudo apt-get upgrade

# Instalamos Nginx
$ sudo apt install nginx

# Descargamos el repositorio de PHP
$ sudo add-apt-repository universe
$ sudo apt install php-fpm php-mysql
$ sudo apt install -y php8.1 php8.1-common php8.1-gd php8.1-intl php8.1-zip php8.1-sqlite3 php8.1-mysql php8.1-fpm php8.1-mbstring php8.1-xml php8.1-curl php8.1-memcached

# Verificamos nuestra version de PHP
$ php -v

# Instalamos MySQL
$ sudo apt install mysql-server

# Ahora, para evitar algunos errores a la hora de pasar a la configuracion de seguridad,
# vamos a asignar una contraseña vacia momentanea a nuestro usuario root

$ sudo mysql
sql> ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password by '';
sql> exit;

# Procedemos a asegurar la instalacion de nuestro mysql
$ sudo mysql_secure_installation

# ***************************-------------------------------***********************************
# Acontinuacion nos preguntara si deseamos que se validen las contraseñas, mediante el plugin
# para esta ocacion, responderemos que no (N), Luego colocamos la contraseña que tendremos por defecto
# en el usuario root para ingresar y confirmamos contraseña

# Seguidamente respondemos a las siguientes preguntas:
# Remove anonymous users? Y
# Disallow root login remotely? Y
# Remove Test database? Y
# Reload Privilege tables now? Y

# con esto finalizamos la instalacion de nuestro MySQL
# ***************************-------------------------------***********************************

# Ahora ingresaremos para crear un usuario y una base de datos para realizar pruebas
$ mysql -u root -p
# Ingresamos nuestra contraseña

sql> CREATE DATABASE test_db;
sql> CREATE USER 'test_user'@'localhost' IDENTIFIED BY '12345678';

# ahora le daremos los privilegios a este usuario sobre la base de datos creada
sql> GRANT ALL PRIVILEGES ON test_db.* TO 'test_user'@'localhost';
sql> exit;

# Podemos realizar una prueba, ingresando con nuestro nuevo usuario
$ mysql -u test_user -p
sql> SHOW DATABASES;

# Deberiamos poder ver nuestra base de datos.
```

## Instalamos NODE JS
```bash
# para instalar NODE JS, podemos ir a la pagina oficial y buscar
# instalacion via manejador de paquetes,
# luego paquetes para la distribucion de ubuntu y encontraremos el repositorio de git
# https://github.com/nodesource/distributions/blob/master/README.md

# Buscamos la version de node deseada, en mi caso es la Node.js v14.x:
# y ejecutamos los comandos que se nos indican
$ curl -fsSL https://deb.nodesource.com/setup_14.x | sudo -E bash -
$ sudo apt-get install -y nodejs

# ahora verificamos nuestras versiones de NODE y NPM
$ node -v
$ npm -v 
# en caso de no instalarse npm, puedes hacerlo con el comando
$ sudo apt install npm
```

## Instalamos COMPOSER
```bash
# para instalar composer, tenemos que instalar algunas dependencias
$ sudo apt install curl php-cli php-mbstring unzip

# instalar composer
$ cd ~
$ curl -sS https://getcomposer.org/installer -o composer-setup.php
$ ls

# instalar composer
$ sudo php composer-setup.php --install-dir=/usr/local/bin --filename=composer

# por ultimo corremos composer para verificar que todo esta correcto
$ composer


# aprovechamos a instalar algunos paquetes que son requeridos por laravel
$ sudo apt install php-mbstring php-xml php-bcmath
```

## Clonaremos nuestro proyecto
```bash
$ cd /var/www

$ mkdir test_laravel
$ cd test_laravel

$ git clone https://gitlab.com/doncanzsa/test-laravel.git .

# instalamos dependencias
$ sudo composer install --ignore-platform-reqs

# vamos a crear una copia de nuestro .env.example

$ cp .env.example .env

# abrimos nuestro archivo .env para modificarlo
$ nano .env

# configuramos las variables
APP_ENV=production
DB_DATABASE=test_db
DB_USERNAME=test_user
DB_PASSWORD=12345678

#--NOTA--------------------
# para salir, precionamos:
# Ctrl + x
# y
# ENTER
# -------------------------

# generamos nuestra clave de aplicacion
$ php artisan key:generate

# generamos nuestro enlace con storage
$ php artisan storage:link

# asignamos permisos de escritura a las carpetas requeridas
$ sudo chown -R www-data.www-data /var/www/test_laravel/storage
$ sudo chown -R www-data.www-data /var/www/test_laravel/bootstrap/cache

# Corremos las migraciones de nuestro proyectp
$ php artisan migrate

# Finalmente vamos a compilar nuestros archivos js
$ npm install
$ npm run dev

```


### CONFIGURAR NGINX
En este punto nuestro servidor se encuentra listo para ser mostrado,
ahora solo depende de la configuración que realicemos en nginx

para poder cubrir un despliegue como lo traemos trabajando, vamos a mostrar nuestra aplicación mediante la IP de nuestro servidor, y posteriormente modificar nuestra configuración para mostrarla mediante un dominio.

```bash
# creamos un archivo de configuracion nginx con el nombre de nuestra aplicacion
$ sudo nano /etc/nginx/sites-available/test_laravel

# para la configuracion puede ver el sitio oficial de LARAVEL
# https://laravel.com/docs/5.8/deployment#server-configuration
# copiamos el codigo que aparece en el sitio, y nos quedara algo similar a

server {
    listen 80;
    server_name __IP_SERVER__;
    root /var/www/test_laravel/public;
 
    add_header X-Frame-Options "SAMEORIGIN";
    add_header X-XSS-Protection "1; mode=block";
    add_header X-Content-Type-Options "nosniff";
 
    index index.html index.htm index.php;
 
    charset utf-8;
 
    location / {
        try_files $uri $uri/ /index.php?$query_string;
    }
 
    location = /favicon.ico { access_log off; log_not_found off; }
    location = /robots.txt  { access_log off; log_not_found off; }
 
    error_page 404 /index.php;
 
    location ~ \.php$ {
        fastcgi_pass unix:/var/run/php/php8.1-fpm.sock;
        fastcgi_index index.php;
        fastcgi_param SCRIPT_FILENAME $realpath_root$fastcgi_script_name;
        include fastcgi_params;
    }
 
    location ~ /\.(?!well-known).* {
        deny all;
    }
}

# En __IP_SERVER__ debes colocar la Ip de tu servidor, la encuentras en digital ocean
# En root colocamos la direccion de nuestro proyecto apuntanto a la carpeta public
# tambien cambiamos la version de php de 7.2 a 8.1

#--NOTA--------------------
# para salir, precionamos:
# Ctrl + x
# y
# ENTER
# -------------------------

# ahora crearemos un enlace simbolico a los sitions habilidatos
$ sudo ln -s /etc/nginx/sites-available/test_laravel /etc/nginx/sites-enabled/

# comprobamos que la sintaxis de nginx es correcta
$ sudo nginx -t

# recargamos la configuración de nginx
$ sudo systemctl reload nginx

# en este momento ya podemos ver nuestra aplicacion mediante la IP de nuestro servidor
```

### Asignando nombre de dominio

Para asignar nuestra aplicación a un nombre de dominio, basta con obtener nuestro dominio,
apuntar a la ip de nuestro servidor, y modificar nuestro archivo de configuración Nginx
y en lugar de __IP_SERVER__ colocar el dominio que llamara por nuestra aplicación.

